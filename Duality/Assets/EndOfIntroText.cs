using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfIntroText : MonoBehaviour
{
    public PlayerMovement ThePlayer;
    public void OnDisable()
    {
        ThePlayer.CanControl = true;
    }
}
