using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialMessage : MonoBehaviour
{
    public Transform[] ChildImages;
    public TextMeshProUGUI ChildText;
    
    public void UpdateTutorialMessage(string message, Sprite[] images)
    {
        gameObject.SetActive(true);
        ChildText.text = message;
        for (var i = 0; i < ChildImages.Length; i++)
        {
            if (i < images.Length)
            {
                ChildImages[i].gameObject.SetActive(true);
                ChildImages[i].GetComponent<Image>().sprite = images[i];
            }
            else
            {
                ChildImages[i].gameObject.SetActive(false);
            }
        }
    }

    public void HideTutorialMessage()
    {
        gameObject.SetActive(false);
    }
}
