using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// Things that have to be done at the start o the level
public class LevelStart : MonoBehaviour
{
    public PlayerMovement ThePlayer;
    public GameObject IntroText;
    
    // Start is called before the first frame update
    void Start()
    {
        IntroText.SetActive(true);
        ThePlayer.CanControl = false;
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
