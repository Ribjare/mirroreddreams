using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfLevel : MonoBehaviour
{

    public GameObject FinalScreen;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D c)
    {
        FinalScreen.SetActive(true);
    }

    public void OnDisable()
    {
        // When the final text ends, it destroys this game object in order to run this block
        // where we load the menu
        LoadManager.LoadLevel("Menu");
    }
}
