﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    #region Sigleton

    public static SceneChanger instance;

    private void Awake()
    {
        if(instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion

    public Animator animator;
    private int levelToLoad;

    public void FadeToNextLevel()
    {
        // FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
        LoadManager.LoadNextLevel();
    }

    public void FadeToLevel(string levelName)
    {
        // levelToLoad = levelIndex;
        //animator.SetTrigger("fadeOut");
        StartCoroutine(Transition(levelName));
    }

    public IEnumerator Transition(string levelName)
    {

        animator.SetTrigger("fadeOut");

        yield return new WaitForSeconds(1f);
        LoadManager.LoadLevel(levelName);
    }

    
    /// <summary>
    /// Method that is invoked by an animation event: when the fade_out animation finishes playing, this method is invoked.
    /// </summary>
    public void OnFadeComplete()
    {
        //SceneManager.LoadScene(levelToLoad);
    }
}
