using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointToggle : MonoBehaviour
{
    // ParticleSystem variables
    private ParticleSystem checkpointLightParticleSystem;
    private ParticleSystem spinningParticles;
    private ParticleSystem spinningParticles2;
    private ParticleSystem middleCheckpointParticlesWhite;
    private ParticleSystem middleCheckpointParticlesBlue;

    // AudioSource variable
    private AudioSource checkpointAudioSource;

    // Lerp variables
    private float timeElapsed;
    private float startValue;
    private float endValue;
    private float valueToLerp;

    // Set the lerpDuation in the inspector
    public float lerpDuration;

    // Toggle variable to start / stop the checkpoint particle systems
    public bool toggleParticleSystems;

    // Variables for the SpriteRenderer components of the sprites
    private SpriteRenderer checkpointSpriteRendererWithShader;
    private SpriteRenderer checkpointSpriteRendererNoShader;

    // Start is called before the first frame update
    void Start()
    {
        // The main goal is to Lerp the value of the Start Lifetime property
        // (in the particle system) so we can make the effect start from
        // the ground up (instead of just appearing instantly)

        // We need to set the correct particle system to a variable
        // to use it in the lerp
        checkpointLightParticleSystem = GameObject.Find("CheckpointLight").GetComponent<ParticleSystem>();

        // Just set the remaining particle systems that don't need special treatment
        spinningParticles = GameObject.Find("SpinningParticles").GetComponent<ParticleSystem>();
        spinningParticles2 = GameObject.Find("SpinningParticles2").GetComponent<ParticleSystem>();
        middleCheckpointParticlesWhite = GameObject.Find("MiddleCheckpointParticlesWhite").GetComponent<ParticleSystem>();
        middleCheckpointParticlesBlue = GameObject.Find("MiddleCheckpointParticlesBlue").GetComponent<ParticleSystem>();

        // Get the AudioSource component to play the sound later
        checkpointAudioSource = GetComponent<AudioSource>();

        // Just init some values... move along, nothing important to see here...
        startValue = 0;
        endValue = 5.0f;

        // This value must start false for each checkpoint
        toggleParticleSystems = false;

        // Set the checkpoint sprite renderer so we can swap images
        checkpointSpriteRendererWithShader = GameObject.Find("CheckpointSpriteWithShader").GetComponent<SpriteRenderer>();
        checkpointSpriteRendererNoShader = GameObject.Find("CheckpointSpriteNoShader").GetComponent<SpriteRenderer>();

        // Turn off the shaded checkpoint sprite
        checkpointSpriteRendererWithShader.enabled = false;
        // Turn on the simple checkpointSprite
        checkpointSpriteRendererNoShader.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(toggleParticleSystems) {
            // Swap the images to the "turned on" checkpoint
            checkpointSpriteRendererNoShader.enabled = false;
            checkpointSpriteRendererWithShader.enabled = true;

            // Let the fireworks start!
            checkpointLightParticleSystem.Play();
            spinningParticles.Play();
            spinningParticles2.Play();
            middleCheckpointParticlesWhite.Play();
            middleCheckpointParticlesBlue.Play();

            // Simple linear lerp
            if (timeElapsed < lerpDuration) {
                valueToLerp = Mathf.Lerp(startValue, endValue, timeElapsed / lerpDuration);
                timeElapsed += Time.deltaTime;
            }
            else {
                valueToLerp = endValue;
            }

            // To use and access the properties from the Particle System, we need to
            // set the main variable this way
            // Ex.: https://docs.unity3d.com/ScriptReference/ParticleSystem.MainModule-startLifetime.html
            var checkpointLightMain = checkpointLightParticleSystem.main;
            var middleCheckpointParticlesWhiteMain = middleCheckpointParticlesWhite.main;
            var middleCheckpointParticlesBlueMain = middleCheckpointParticlesBlue.main;

            checkpointLightMain.startLifetime = valueToLerp;
            middleCheckpointParticlesWhiteMain.startLifetime = valueToLerp;
            middleCheckpointParticlesBlueMain.startLifetime = valueToLerp;

        }
        else {
            // Swap the images back to the "turned off" checkpoint
            checkpointSpriteRendererNoShader.enabled = true;
            checkpointSpriteRendererWithShader.enabled = false;

            // Stop all the fireworks
            checkpointLightParticleSystem.Stop();
            spinningParticles.Stop();
            spinningParticles2.Stop();
            middleCheckpointParticlesWhite.Stop();
            middleCheckpointParticlesBlue.Stop();

            // Initialize the startLifetime property with the starting value
            var checkpointLightMain = checkpointLightParticleSystem.main;
            var middleCheckpointParticlesWhiteMain = middleCheckpointParticlesWhite.main;
            var middleCheckpointParticlesBlueMain = middleCheckpointParticlesBlue.main;

            checkpointLightMain.startLifetime = startValue;
            middleCheckpointParticlesWhiteMain.startLifetime = startValue;
            middleCheckpointParticlesBlueMain.startLifetime = startValue;
            // Init the timeElapsed variable so we can startover if we want
            timeElapsed = 0;
        }
    }

	private void OnTriggerEnter2D(Collider2D collision) {
        // Mudar a string da tag de acordo com a tag existente na Eve
        // if(other.CompareTag("Player")) {
            checkpointSpriteRendererNoShader.enabled = false;
            checkpointSpriteRendererWithShader.enabled = true;
            toggleParticleSystems = true;
            // Ligar o som
            checkpointAudioSource.Play();
        //}
    }
}
