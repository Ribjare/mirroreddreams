using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CheatManager))]
public class NeverevenCheats : MonoBehaviour
{
    private CheatManager cm;

    public PlayerMovement ThePlayerMovement;
    public TimeControl TheTimeControl;
    
    void Start()
    {
        cm = GetComponent<CheatManager>();
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.anyKeyDown)
        {
            if (cm.ApplyCheats("EVEREWIND"))
            {
                TheTimeControl.PowerCanRewind = !TheTimeControl.PowerCanRewind;
            }
            
            if (cm.ApplyCheats("EVEFF"))
            {
                TheTimeControl.PowerCanFastForward = !TheTimeControl.PowerCanFastForward;
            }
            
        }
        
    }
}
