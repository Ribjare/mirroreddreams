using System.Collections;
using System.Collections.Generic;
using Chronos;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{

    [SerializeField] private GameObject menuGameObject;
    [SerializeField] private GameObject defaultSelectedItem;
    [SerializeField] private KeyCode[] Keys;

    private bool pressedOne()
    {
        foreach (KeyCode c in Keys)
        {
            if (Input.GetKeyDown(c))
            {
                return true;
            }
        }

        return false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (pressedOne())
        {
            ToggleMenu();
        }
    }

    private bool IsPaused()
    {
        return (menuGameObject.activeSelf);
    }
    
    public void ToggleMenu()
    {
        if (IsPaused())
        {
            ResumeGame();
        }
        else
        {
            //NotebookHighlight.Instance.isBlinking = false;
            PauseGame();
            menuGameObject.SetActive(true);
            if (defaultSelectedItem != null)
            {
                GetComponent<MenuOptions>().SelectUIElement(defaultSelectedItem);
            }
        }
    }


    
    public void PauseGame()
    {
        Cursor.visible = true;

        //Time.timeScale = 0;
        Timekeeper.instance.Clock("PlayerClock").paused = true;
        Timekeeper.instance.Clock("EverythingElseClock").paused = true;
    }

    public void ResumeGame()
    {
        Cursor.visible = false;

        //Time.timeScale = 1;
        Timekeeper.instance.Clock("PlayerClock").paused = false;
        Timekeeper.instance.Clock("EverythingElseClock").paused = false;        
        
        menuGameObject.SetActive(false);
    }
}
