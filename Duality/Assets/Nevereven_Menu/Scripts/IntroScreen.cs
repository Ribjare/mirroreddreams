
using UnityEngine;
using UnityEngine.EventSystems;

public class IntroScreen : MonoBehaviour
{

    public GameObject HideThis;
    public GameObject ShowThis;
    public GameObject SelectThis;
    
    void Update()
    {

        if (Input.anyKeyDown)
        {
            HideThis.SetActive(false);
            ShowThis.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(SelectThis);
        }
        
    }
}
