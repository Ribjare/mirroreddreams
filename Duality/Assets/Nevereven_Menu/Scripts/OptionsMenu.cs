using System;
using System.Linq;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class OptionsMenu : MonoBehaviour
{
    public Toggle FullScreen;
    
    
    
    private int currentResolution;

    public TextMeshProUGUI ResolutionLabel;

    public AudioMixer TheMixer;

    public Slider MasterSlider;
    
    public Slider MusicSlider;
    
    public Slider SFXSlider;


    public TextMeshProUGUI MasterLabel;

    public Text MusicLabel;

    public Text SFXLabel;


    public TextMeshProUGUI LanguageText;

    public string[] AvailableLanguages;

    public int CurrentIndexLanguage;


	public void UpdateControlValues()
    {
        // Reads values and sets defaults if needed
        
        MasterSlider.value = 
            PlayerPrefs.HasKey("MasterVolume")? 
                PlayerPrefs.GetFloat("MasterVolume"): 
                80;
        
        FullScreen.isOn = 
            PlayerPrefs.HasKey("FullScreen")? 
            PlayerPrefs.GetInt("FullScreen")==1: 
            Screen.fullScreen;
        
        ResolutionLabel.text = 
            PlayerPrefs.HasKey("Resolution") ? 
            PlayerPrefs.GetString("Resolution") : 
            ResolutionToString(Screen.currentResolution.width, Screen.currentResolution.height);
        
        LanguageText.text = 
            PlayerPrefs.HasKey("Language") ? 
                PlayerPrefs.GetString("Language") : 
                "EN";



        
        currentResolution = -1;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            if (ResolutionToString(Screen.resolutions[i].width, Screen.resolutions[i].height) == ResolutionLabel.text)
            {
                currentResolution = i;
                break;
            }
        }
        
        
        if (currentResolution == -1)
        {
            Debug.LogWarning("What the hell happened here with resolutions?");
            // This happens when we read a resolution from the player prefs and that resolution is not in the list
            // of available resolutions (changed computer? changed screen? screwing with the values in regedit?)
            // or the system starts with a default resolution that is not listed as one of the available resolutions
            // (is this even possible?)
            
            // Sets as default resolution the last on the list (probably the best)
            currentResolution = Screen.resolutions.Length - 1;
        }

        UpdateResolutionLabel();




        CurrentIndexLanguage = AvailableLanguages.ToList().IndexOf(LanguageText.text);
        if (CurrentIndexLanguage < 0)
        {
            CurrentIndexLanguage = 0;
        }
        UpdateCurrentLanguageText();
        
        
        
        
        // Updates system with the settings
        
        Screen.SetResolution(
            Screen.resolutions[currentResolution].width, 
            Screen.resolutions[currentResolution].height,
            FullScreen.isOn);
        
        SetMasterVolume();

    }
        
    

    public static string ResolutionToString(int w, int h)
    {
        return w + " X " + h;
    }
    
    public void ApplyGraphics()
    {
        Screen.SetResolution(
            Screen.resolutions[currentResolution].width, 
            Screen.resolutions[currentResolution].height,
            FullScreenMode.FullScreenWindow);
        
        Screen.fullScreen = FullScreen.isOn;
        //QualitySettings.vSyncCount = test?1:0;

        PlayerPrefs.SetInt("FullScreen", FullScreen.isOn ? 1 : 0);
        
        PlayerPrefs.SetString("Resolution", 
            ResolutionToString(
                Screen.resolutions[currentResolution].width, 
                Screen.resolutions[currentResolution].height));
    }

    public void NextResolution()
    {

        currentResolution++;
        if (currentResolution >= Screen.resolutions.Length)
        {
            currentResolution = Screen.resolutions.Length - 1;
        }

        UpdateResolutionLabel();
    }

    public void PreviousResolution()
    {
        currentResolution = currentResolution > 0 ? currentResolution - 1: 0;
        UpdateResolutionLabel();
    }


    public void UpdateCurrentLanguageText()
    {
        LanguageText.text = AvailableLanguages[CurrentIndexLanguage];
        PlayerPrefs.SetString("Language", LanguageText.text);
        ParseLanguageCSV.Instance.Language = LanguageText.text;
        ParseLanguageCSV.Instance.LoadLanguage();
        ParseLanguageCSV.UpdateAllTranslations();
    }
    
    public void NextLanguage()
    {
        CurrentIndexLanguage++;
        if (CurrentIndexLanguage >= AvailableLanguages.Length)
        {
            CurrentIndexLanguage = AvailableLanguages.Length - 1;
        }
        UpdateCurrentLanguageText();
    }

    public void PreviousLanguage()
    {
        // Showing off a ternary operator (it saves an operation in extreme condition)
        CurrentIndexLanguage = CurrentIndexLanguage > 0 ? CurrentIndexLanguage - 1: 0;
        UpdateCurrentLanguageText();
    }
    
    
    
    public void UpdateResolutionLabel()
    {
        ResolutionLabel.text = Screen.resolutions[currentResolution].width + " X " +
            Screen.resolutions[currentResolution].height;

    }

    public void SetMasterVolume()
    {
        // After the recent changes to the menu, we decided to remove the text value
        // from the slider. However, we don't what the future holds... so I'll just
        // comment this line because we might need it further down the road.
        // MasterLabel.text = MasterSlider.value.ToString(CultureInfo.InvariantCulture);
        TheMixer.SetFloat("MasterVolume", PercentageToDecibels(MasterSlider.value));
        PlayerPrefs.SetFloat("MasterVolume", MasterSlider.value);
    }

    public float PercentageToDecibels(float percentage)
    {
        // From -80 to 20 

        if (percentage < 2.0f)
        {
            return -10000f;
        }
        return 10f * Mathf.Log(percentage/100f) + 2f;
        
        //return (percentage - 95);
    }
    
}
