using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuOptions : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayScene(string scene)
    {
        //SceneManager.LoadScene(scene);
        //LoadManager.LoadLevel(scene);
        SceneChanger.instance.FadeToLevel(scene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OpenScreen(GameObject g)
    {
       g.SetActive(true);
    }

    public void SelectUIElement(GameObject g)
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(g);
    }
    

    public void CloseScreen(GameObject g)
    {
        g.SetActive(false);
    }
    
    
}
