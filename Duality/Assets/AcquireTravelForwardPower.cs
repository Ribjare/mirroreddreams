using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcquireTravelForwardPower : MonoBehaviour
{
    
    public TimeControl TheTimeControl;
    public GameObject TheRock; // The one that is rolling towards the players, not Dwayne Johnson
    public PlayerMovement ThePlayerMovement;

    private float initialAngular;
    
    private void Start ()
    {
        TheTimeControl.PowerCanFastForward = true;
        TheRock.GetComponent<MovableBlock>().ActiveMovement = false;
        initialAngular = TheRock.GetComponentInChildren<RotateAroundAPoint>().angularSpeed;
        TheRock.GetComponentInChildren<RotateAroundAPoint>().angularSpeed = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            TheRock.GetComponent<MovableBlock>().ActiveMovement = true;
            TheRock.GetComponentInChildren<RotateAroundAPoint>().angularSpeed = initialAngular;
            ThePlayerMovement.CanControl = true;
            Invoke("DestroySelf", 1.0f);
        }
    }

    public void DestroySelf()
    {
        Destroy(this);
    }
}
