using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IntroTextAndSound : MonoBehaviour
{
    public AudioClip Audio;
    public string TextKey;

    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = ParseLanguageCSV.GetString(TextKey);
        GetComponent<BasicFade>().MainTime = Audio.length;
        PlaySound();
    }

    void PlaySound()
    {
        GetComponentInParent<AudioSource>().clip = Audio;
        GetComponentInParent<AudioSource>().Play();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<BasicFade>().EndEarly();
        }
    }
}
