using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTabPress : MonoBehaviour
{

    public GameObject Book;
    public GameObject SecondMessage;
    public PlayerMovement TheCharacter;
    public GameObject CollectiblesPanel;
    
    private int count = 0;

    
    void Start()
    {
        CollectiblesPanel.SetActive(true);
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            count++;
            //Destroys the book
            //Destroys self
            //Destroys message
            
            // Basically, the whole game object (and the book) and call it a day
            if (count == 1)
            {
                Destroy(Book);
                Destroy(GetComponent(typeof (CircleCollider2D)));
            }
            
            else if (count == 2)
            {
                SecondMessage.SetActive(true);
            }
            
        }
    }
}
