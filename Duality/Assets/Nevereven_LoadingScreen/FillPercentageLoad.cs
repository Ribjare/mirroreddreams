﻿using UnityEngine;
using UnityEngine.UI;

public class FillPercentageLoad : MonoBehaviour {

    private Image ImageComponent;


    void Start ()
    {
        ImageComponent = GetComponent<Image>();
	}
	


	void Update ()
    {
        // Is this really needed
        // AFAIK null only happens if the desired load scene is not added to the list of scenes in unity
        // Probably the exception is better than making the error with a steady locked screen
        if (LoadManager.Operation != null)
        {
            ImageComponent.fillAmount = LoadManager.Operation.progress * 1.15f;
        }
        else
        {
            ImageComponent.fillAmount = 0;
        }

    }
}
