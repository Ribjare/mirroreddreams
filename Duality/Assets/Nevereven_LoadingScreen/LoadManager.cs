﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager
{
    // The name of the scene that is used as loading screen
    private static readonly string LoadSceneName = "LoadingScene";

    // Stores the level to be loaded (after this scene)
    private static string NextLevel = "Menu";



    public static AsyncOperation Operation
    {
        get; private set;
    }
    
    
    
    // Send a command to load a new level
    public static void LoadLevel (string nextLevel)
    {
        // Clears the old operation (previous usage of load manager)
        // This is new (2021) stuff
        // because I was getting an error regarding the access to Operation before the entire load of this temp scene
        Operation = null;
        NextLevel = nextLevel;

         SceneManager.LoadScene(LoadSceneName);
    }
    public static void ReloadLevel ()
    {
        LoadLevel(SceneManager.GetActiveScene().name);
    }

    // Goes to the desired level
    public static void LoadNextLevel ()
    {
       
        Operation = SceneManager.LoadSceneAsync (NextLevel);
    }

}
