using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconRotate : MonoBehaviour
{

    // Rotation per second
    [SerializeField] private Vector3 RotationVector;
    
    
    void Update()
    {
        // Very complex script that multiplies a vector with the elapsed time
        transform.Rotate(RotationVector * Time.deltaTime);
    }
}
