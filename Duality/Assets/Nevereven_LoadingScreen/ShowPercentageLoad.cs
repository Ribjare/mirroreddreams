﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowPercentageLoad : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (LoadManager.Operation != null)
		{
			GetComponent<Text>().text = ((int) (LoadManager.Operation.progress * 111.5f)).ToString();
		}

	}
}
