using System.Collections;
using System.Collections.Generic;
using Chronos;
using UnityEngine;




// Pensar em fazer um script genérico para este comportamento (Activate And Destroy)

public class ActivateCheckpoint : MonoBehaviour
{
    public GameObject ColliderToActivate;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter2D()
    {
        // Activates the collider that presents the player from getting back
        ColliderToActivate.SetActive(true);
        
        // Destroys the trigger just because we don't need it anymore
        // Performance, bugs, whatever... it is just better to destroy
        Destroy(gameObject);
        
        // Resets all clocks
        // This has so much potential to screw up
        foreach (var t in FindObjectsOfType<Timeline>())
        {
            t.ResetRecording();
        }
    }
}
