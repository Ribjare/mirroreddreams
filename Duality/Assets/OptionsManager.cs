using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsManager : MonoBehaviour
{
    [SerializeField] private OptionsMenu optionsMenu;
    
    // Start is called before the first frame update
    void Start()
    {
        optionsMenu.AvailableLanguages = ParseLanguageCSV.Instance.ExistingLanguages;
        optionsMenu.UpdateControlValues();
    }

 
}
