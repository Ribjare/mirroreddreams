using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BasicFade : MonoBehaviour
{
    public float FadeInTime;
    public float FadeOutTime;
    public float MainTime;

    private float currentTime;

    public GameObject ActivateAtTheEnd;
    public GameObject DeactivateAtTheEnd;

    public MonoBehaviour[] ComponentsToFade;
    
    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0.0f;
        if (FadeInTime > 0.0)
        {
            FadePercentage(0.0f);
        }
        else
        {
            FadePercentage(1.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime < FadeInTime)
        {
            FadePercentage(currentTime/FadeInTime);
        }
        else if (currentTime < FadeInTime + MainTime)
        {
            FadePercentage(1.0f);
        }
        else if (currentTime < FadeInTime + MainTime + FadeOutTime)
        {
            FadePercentage(1 - ((currentTime - MainTime - FadeInTime)/FadeOutTime));
        }
        else
        {
            Destroy(gameObject);
            if (ActivateAtTheEnd != null)
            {
                ActivateAtTheEnd.SetActive(true);
            }
            
            if (DeactivateAtTheEnd != null)
            {
                DeactivateAtTheEnd.SetActive(false);
            }
        }
    }

    public void FadePercentage(float f)
    {
        foreach (var c in ComponentsToFade)
        {
            if (c.GetType() == typeof(TextMeshProUGUI))
            {
                Color32 color = ((TextMeshProUGUI) c).faceColor;
                color.a = (byte) (f * 255.0f);
                ((TextMeshProUGUI) c).faceColor = color;
            }
            if (c.GetType() == typeof(Image))
            {
                Color32 color = ((Image) c).color;
                color.a = (byte) (f * 255.0f);
                ((Image) c).color = color;
            }
            else
            {
                // Don't know how to fade this
            }
        }
    }

    public void EndEarly()
    {
        currentTime = FadeInTime + FadeOutTime + MainTime + 0.1f;
        Update();
    }
    
    
    
}
