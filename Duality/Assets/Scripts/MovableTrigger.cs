
using UnityEngine;

public class MovableTrigger : MonoBehaviour
{
    // My conscious says this should be an array of Commands in which a command has an action and a block
    // To to in 2022;

    [SerializeField] private MovableBlock movableBlock;
    [SerializeField] private TriggerActions action;

    private void OnTriggerEnter2D(Collider2D other)
    {
        movableBlock.Trigger(action);
    }

}
