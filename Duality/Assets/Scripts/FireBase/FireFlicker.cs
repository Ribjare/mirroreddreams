using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class FireFlicker : MonoBehaviour
{
    // Public methods to show in Inspector
    public enum METHOD { PerlinNoise, LinearRandom }
    public METHOD method;

    public float minIntensity = 5.0f;
    public float maxIntensity = 10.0f;
    public float timeMultiplier = 0.5f;

    public bool active;

    // Class variables
    private Light2D light2d;
    private float randomValue;
    private float noise = 0.0f;
    private float targetIntensity = 1.0f;
    private float lastIntensity = 1.0f;
    private float timePassed = 0.0f;

    private const double tolerance = 0.0001;

    // Start is called before the first frame update
    void Start()
    {
        // Init values
        randomValue = Random.Range(0.0f, 65535.0f);
        // Cache light value
        light2d = gameObject.GetComponent<Light2D>();
        lastIntensity = light2d.intensity;
        // Set the default method as Linear Random
        method = METHOD.LinearRandom;
        // Is the flicker effect active?
        active = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Only check the flicker method if the effect is active
        if(active) {
            // Check the selected method and use code accordingly

            // This first method uses the PerlinNoise function to return a value between
            // 0 and 1, according to the random value that is passed as an argument.
            // It then lerps the intensity values using the noise returned value with
            // a time multiplier
            if(method == METHOD.PerlinNoise) {
		        noise = Mathf.PerlinNoise(randomValue, Time.time);
		        light2d.intensity = Mathf.Lerp(minIntensity, maxIntensity, noise / timeMultiplier);
		    }
            // This method uses a lerp to change the light intensity and if the difference
            // between the two intensity values (current and target intensity) is inside
            // the tolerance value, new random values are generated to be used in the lerp
            else if(method == METHOD.LinearRandom) {
                timePassed += Time.deltaTime;
                light2d.intensity = Mathf.Lerp(lastIntensity, targetIntensity, timePassed / timeMultiplier);

                if (Mathf.Abs(light2d.intensity - targetIntensity) < tolerance) {
                    lastIntensity = light2d.intensity;
                    targetIntensity = Random.Range(minIntensity, maxIntensity);
                    timePassed = 0.0f;
                }
		    }
		}
        // If the flicker is not active, we use the minimum intensity value
        else {
            light2d.intensity = minIntensity;
        }
    }
}
