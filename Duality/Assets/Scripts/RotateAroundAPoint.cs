using Chronos;
using UnityEngine;


/// <summary>
/// Script that makes an object rotate around itself at a specific speed
/// 
/// AngluarSpeed > 0 : Rotates right
/// AngularSpeed < 0 : Rotates left
/// 
/// </summary>
public class RotateAroundAPoint : MonoBehaviour
{

	[SerializeField]
	[Tooltip("Rotation Speed, > 0 : Rotates Rigth; < 0 : Rotates Left")]
	public float angularSpeed = 2f;

	// Update is called once per frame
	void Update()
	{

		var angle = GetComponent<Timeline>().deltaTime * angularSpeed;

		transform.Rotate(0, 0, angle, Space.Self);

	}
}
