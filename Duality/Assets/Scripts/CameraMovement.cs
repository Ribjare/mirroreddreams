using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private List<GameObject> characters;
    [SerializeField] private List<GameObject> cameras;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        var averageX = characters.Average(c => c.transform.position.x);
        foreach (var c in cameras)
        {
            c.transform.position = new Vector3(averageX, c.transform.position.y, c.transform.position.z);
        }

    }
}
