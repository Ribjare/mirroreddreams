
using Chronos;
using UnityEngine;


// A "list" with the used animations
public enum ExistingAnimations
{
    ANIM_IDLE,
    ANIM_RUN,
    ANIM_FALL,
    ANIM_JUMP
}

public class PlayerMovement : MonoBehaviour
{
    // Cache for Rigid Body
    private Rigidbody2D _rb;

    // Leave null if there is an animator in the GameObject
    [SerializeField] private Animator _animator;


    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();

        // Caches the animator
        if (_animator == null)
        {
            _animator = GetComponent<Animator>();
        }

        // And claims to be alive at start. Fair assumption. 
        Alive = true;

    }


    // Controller configuration

    [SerializeField] private float jumpPower = 2.0f;

    [SerializeField] private float runSpeed = 5f;

    [SerializeField] private bool doubleJumpEnabled = false;

    [SerializeField] private float JumpDamp = 2.0f;

    [SerializeField] private float jumpBufferTime = 0.1f;

    [SerializeField] private float coyoteTime;

    [SerializeField] private float slideSpeed = 20;


    // Internal attributes for Character Control

    private bool _canAirJump;

    private float _jumpCommandTime;
    private float _groundedTime;



    // Internal attributes for Input Control

    private bool _jumpCommand;
    private bool _leftCommand;
    private bool _rightCommand;
    private bool _idleCommand;

    private bool _jumpEnded;



    private bool _canJump;


    // Stores continuously the animation that is in used
    // (important to play animations backwards while rewinding)
    public ExistingAnimations currentAnimation;


    // Ground control (to Major Tom)

    private bool isGrounded;
    [SerializeField] private GameObject groundTestLineStart;
    [SerializeField] private GameObject groundTestLineEnd;


    // Self explanatory, right?
    public bool Alive { get; private set; }


    // Player can control the character
    // Is set to false in cut scenes and similar situations
    public bool CanControl;
    
    
    
    
    // Because travelling forward is not a "real" time-based power,
    // we keep track of this value
    public bool isFastingForward;


    // Corrigir para encapsular
    private float ffStartTime;

    [Header("Material")]
    public Material materialHologram;
    public Material originalMaterial;

    
  
    public void StartForwarding(float currentTimeInstant)
    {
        ffStartTime = currentTimeInstant;
    }



    private void DisablePhysics()
    {
        GetComponent<CapsuleCollider2D>().isTrigger = true;
        GetComponent<Rigidbody2D>().simulated = false;
    }


    private void EnablePhysics()
    {
        // F: I'm 99% sure that the following ifs are useless
        // F: but the remaining 1% made me put them there just in case
        // F: constantly setting a collider to true may cause some interference in physics calculations (probably not)
        if (GetComponent<CapsuleCollider2D>().isTrigger)
        {
            GetComponent<CapsuleCollider2D>().isTrigger = false;
        }

        if (!GetComponent<Rigidbody2D>().simulated)
        {
            GetComponent<Rigidbody2D>().simulated = true;
        }
    }


    private void Update()
    {

        // Character is dead
        if (!Alive)
        {
            // Ah ah you're dead!
            _animator.SetFloat("AnimationSpeed", Timekeeper.instance.Clock("MainTime").localTimeScale);
            return;
        }


        // Character is fast forwarding (updated by the Time Controller)
        if (TimeControl.Instance.CurrentPower == TimePower.TRAVEL_FORWARD)
        {
            // Updates inner attribute...
            isFastingForward = true;

            // ...and adapts features for the travel forward effect
            UpdateFastFowardEffect();
            DisablePhysics();
        }


        // Character is rewindind or self-rewinding (updated by the Time Controller)
        // F: By now I decided to merge the two states on a single if for ease of maintenance
        // F: The powers are mostly the same as the main difference is that one puts all
        // F: timelines moving backwards and the other one puts the player's timeline.
        // F: This script only cares about the player and thus both powers are equal
        else if (TimeControl.Instance.CurrentPower == TimePower.SELF_REWIND ||
                 TimeControl.Instance.CurrentPower == TimePower.REWIND)
        {
            // Disable main physics
            DisablePhysics();

            // Updates the animator with information about current animation
            // While rewinding, the GameObject object also rewinds the currentAnimation attribute
            _animator.SetFloat("AnimationSpeed", -1.0f);
            UpdateAnimationVariables();

            // Updates the effects in case of a previous usage of the travel forward power
            UpdateFastFowardEffect();
        }


        // The world is paused
        else if (TimeControl.Instance.CurrentPower == TimePower.PAUSE)
        {
            isFastingForward = false;
        }


        // Regular boring movement without any power
        else
        {
            isFastingForward = false;


            // F: This may cause problems on a slow down (where animation speed is not 1)
            _animator.SetFloat("AnimationSpeed", 1.0f);
            // F: Probably is more accurate to do this:
            //_animator.SetFloat("AnimationSpeed", GetComponent<Timeline>().timeScale);
            // but Im not even trying to change... it's working as it is.


            EnablePhysics();


            if (CanControl)
            {

                // No jumps in the demo!!!
                
                // Movement
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _jumpCommand = true;
                    _jumpCommandTime = Time.unscaledTime;
                
                    // F: In a prior prototype was like this:
                    //_jumpCommandTime = GetComponent<Timeline>().unscaledTime;
                    // F: The jump command time has nothing to do with the concept of time within the game
                    // F: It has to do with intervals between key presses,  dso it uses standard unity Time class
                }
                
                

                if (Input.GetKeyUp(KeyCode.Space))
                {
                    _jumpEnded = true;
                }

                _idleCommand = false;

                if (Input.GetKey(KeyCode.A))
                {
                    _leftCommand = true;
                }

                else if (Input.GetKey(KeyCode.D))
                {
                    _rightCommand = true;
                }

                else
                {
                    _idleCommand = true;
                }
            }
            else
            {
                _idleCommand = true;
            }
        }

        // No matter what, update the effect of the player regarding the travel forward power
        UpdateFastFowardEffect();

    }


    // Stores the current "transparency" of the player
    // Transparency = Travel forward effect
    public float CalculatedForwardEffect = 1.0f;

    [SerializeField] private float ForwardEffectMinValue = 0.2f;
    [SerializeField] private float ForwardEffectMaxValue = 1.0f;
    [SerializeField] private float effectSpeed = 2.5f;

    private void UpdateFastFowardEffect()
    {
        // F: Replaced the bottom code with this one just to show off
        // F: Optimizing code while racing for a short deadline... good idea indeed!
        SetOpacityTo(isFastingForward ? ForwardEffectMinValue : ForwardEffectMaxValue);
        ApplyOpacity();

        // if (isFastingForward)
        // {
        //     SetOpacityTo(ForwardEffectMinValue);
        // }
        //
        // else
        // {
        //     SetOpacityTo(ForwardEffectMaxValue);
        //     ApplyOpacity();
        // }
    }


    // F: I'm pretty sure that this could be done differently with Lerp but I'm not risking
    // F: because of the time-based behaviour
    private void SetOpacityTo(float f)
    {
        if (CalculatedForwardEffect > f)
        {
            CalculatedForwardEffect -= Timekeeper.instance.Clock("MainTime").deltaTime * effectSpeed;
        }
        else
        {
            CalculatedForwardEffect += Timekeeper.instance.Clock("MainTime").deltaTime * effectSpeed;
        }

        CalculatedForwardEffect = Mathf.Clamp(CalculatedForwardEffect, ForwardEffectMinValue, ForwardEffectMaxValue);
    }


    private void ApplyOpacity()
    {
        var allChildSpriteRenderes = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sr in allChildSpriteRenderes)
        {
            //sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, CalculatedForwardEffect);
            // To update shader dissolved
            sr.material.SetFloat("_Fade", CalculatedForwardEffect);
        }
    }

    //Changes the origianl material to the hologram one and vice versa
    // Recives a bool to chose witch material we want
    // true for hologram
    // false for original
    // D:Test method, I know this is a mess if we scale
    public void ChangeMaterialHologram(bool t)
    {
        var allChildSpriteRenderes = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sr in allChildSpriteRenderes) {
            if (t) {
                sr.material = materialHologram;
            }else
            {
                sr.material = originalMaterial;
            }
        }
    }



    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(groundTestLineStart.transform.position, groundTestLineEnd.transform.position);
    }
 


    private void FixedUpdate()
    {

        // If the player is somehow rewinding, then don't do the fixed update
        if (GetComponent<Timeline>().timeScale < 0)
        {
            return;
        }


        ExistingAnimations newAnimation = currentAnimation;
        if (_idleCommand)
        {
            newAnimation = ExistingAnimations.ANIM_IDLE;
        }


        if (!isGrounded)
        {
            if (_rb.velocity.y > 0)
            {
                newAnimation = ExistingAnimations.ANIM_JUMP;
            }
            else
            {
                newAnimation = ExistingAnimations.ANIM_FALL;
            }
        }

        var a = Physics2D.Linecast(groundTestLineEnd.transform.position,
                        groundTestLineStart.transform.position);

//        print(a.transform.name);
        
        isGrounded = a;
        ///Debug.Log(a.collider.tag);//---------------------------
        if (isGrounded)
        {
            _groundedTime = Time.unscaledTime;
            _canJump = true;

        }
        else
        {
            if (Time.unscaledTime - _groundedTime > coyoteTime)
            {
                _canJump = false;
            }
        }

        if (_jumpCommand && _canJump)
        {
            _jumpCommand = false;
            if (ShouldJump())
            {
                _rb.velocity = new Vector2(_rb.velocity.x, jumpPower);
                _canAirJump = true;
            }
        }

        if (doubleJumpEnabled && _jumpCommand && !isGrounded && _canAirJump && _rb.velocity.y < 0)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, jumpPower);
            _jumpCommand = false;
            _canAirJump = false;
        }

        if (_jumpEnded)
        {
            Vector2 v = new Vector2(_rb.velocity.x, _rb.velocity.y / JumpDamp);
            _rb.velocity = v;
            _jumpEnded = false;
            _canJump = false;
        }


        if (_leftCommand || _rightCommand)
        {
            _rb.velocity = new Vector2(runSpeed * (_leftCommand ? -1 : 1), _rb.velocity.y);

            transform.localScale = new Vector3((_leftCommand ? -transform.localScale.y : transform.localScale.y), transform.localScale.y, transform.localScale.z);

            if (isGrounded)
            {
                newAnimation = ExistingAnimations.ANIM_RUN;
            }

            _leftCommand = false;
            _rightCommand = false;
        }
        else
        {
            //If Player is free falling
            if (a && a.collider.CompareTag("Slide"))
            {
                _rb.velocity = new Vector2(0, slideSpeed);

            }
            //To fix fastfoward gravity build up, there is a check that nullify the gravity
            if (isFastingForward)
            {
                _rb.velocity = new Vector2(0, 0);

            }
            else
            {
                _rb.velocity = new Vector2(0, _rb.velocity.y);
            }

        }


        SetAnimation(newAnimation);
        UpdateAnimationVariables();

    }


    private bool ShouldJump()
    {
        return Time.unscaledTime - _jumpCommandTime < jumpBufferTime;
    }


    private void SetAnimation(ExistingAnimations a)
    {
        if (currentAnimation == a) return;
        currentAnimation = a;

        // This method made sense before when the animation was controlled directed as shown below
        // Right now we just change a variable and the previous block is even redundant
        //_animator.Play(a);
        //UpdateAnimationVariables();
    }


    private void UpdateAnimationVariables()
    {
        _animator.SetBool("GoIdle", currentAnimation == ExistingAnimations.ANIM_IDLE);
        _animator.SetBool("GoRun", currentAnimation == ExistingAnimations.ANIM_RUN);
        _animator.SetBool("GoJump", currentAnimation == ExistingAnimations.ANIM_JUMP);
        _animator.SetBool("GoFall", currentAnimation == ExistingAnimations.ANIM_FALL);
    }


    public void Kill()
    {
        Alive = false;
    }


}
