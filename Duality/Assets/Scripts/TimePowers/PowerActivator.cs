using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerActivator : MonoBehaviour
{
    [SerializeField]
    private TimePower powerToActivate;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (powerToActivate == TimePower.REWIND)
        {
            ActivatePowerRewind();

        } else if (powerToActivate == TimePower.TRAVEL_FORWARD)
        {
            ActivatePowerForward();
        }
    }

    private void ActivatePowerRewind()
    {
        TimeControl.Instance.PowerCanRewind = true;
    }
    private void ActivatePowerForward()
    {
        TimeControl.Instance.PowerCanFastForward = true;
    }
    private void ActivatePowerPause()
    {
        TimeControl.Instance.PowerCanPause = true;
    }
    private void ActivatePowerSelfRewind()
    {
        TimeControl.Instance.PowerCanSelfRewind = true;
    }

}
