public enum TimePower
{
    NONE,
    TRAVEL_FORWARD,
    PAUSE,
    REWIND,
    SELF_REWIND
}
