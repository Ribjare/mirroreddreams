using System.Collections;
using System.Collections.Generic;
using Chronos;
using UnityEngine;

public class TimerTrigger : MonoBehaviour
{
    [SerializeField] private MovableBlock movableBlock;
    [SerializeField] private TriggerActions action;
    public float Delay;

    private void Start()
    {
    }
    
    private void Update()
    {
        if (movableBlock.GetComponent<Timeline>().time > Delay)
        {
            movableBlock.Trigger(action);
        }
    }
}
