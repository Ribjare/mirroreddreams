
using Chronos;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class TimeControl : MonoBehaviour
{

    // The name of the main clock
    [SerializeField] private string mainClockName;

    // Player Gameobject
    [SerializeField] private GameObject thePlayer;

    // Powers (to enable and disable during the game)
    public bool PowerCanRewind;
    public bool PowerCanFastForward;
    public bool PowerCanSelfRewind;
    public bool PowerCanPause;


    // Singleton instance
    public static TimeControl Instance { get; private set; }


    // Keeps track of the used power (the player may access this)
    public TimePower CurrentPower { get; private set; }

    // Keeps track of the power that was in use in the previous update
    // This might be useful when a certain action is required in power transitions
    public TimePower PreviousPower { get; private set; }

    private bool _changedPower;
    private float _lastPowerChangeTime;


    // Slowdown time when dead factor (percentage for each calculation)
    // Between 0 and 1
    // 0 means instant pause
    // 1 means no slow down
    // more than 1 increases speed which is just dumb
    public float SlowdownTimeFactor = 0.9999f;

    private int ffCount = 0;
    private float ffTime;

    void Start()
    {
        // Singleton stuff
        if (Instance != null)
        {
            Debug.LogWarning("You're messing up your Time Control Singleton!");
        }
        Instance = this;

    }

    // Update is called once per frame
    void Update()
    {
        // Backs up the info about the power that used previously...
        PreviousPower = CurrentPower;
        // ... and assumes that now we are not using any power (the rest of the code may change this)
        CurrentPower = TimePower.NONE;


        
        
        
        
        //The player is dead (poor one)
        // if (!thePlayer.GetComponent<PlayerMovement>().Alive)
        // {
        //     // This code may be an approach to do a fast slow down instead of an abrupt pause
        //     if (!Timekeeper.instance.Clock(mainClockName).paused && 
        //         Timekeeper.instance.Clock(mainClockName).localTimeScale > 0)
        //     {
        //         
        //         // Old code to slow down when dying
        //         
        //         //Timekeeper.instance.Clock(mainClockName).localTimeScale *= SlowdownTimeFactor;
        //         
        //         //if (Timekeeper.instance.Clock(mainClockName).localTimeScale < 0.01f)
        //         {
        //             Timekeeper.instance.Clock(mainClockName).paused = true;
        //             Timekeeper.instance.Clock(mainClockName).localTimeScale = 0;
        //         }
        //
        //         
        //         // Vignette effect
        //         
        //         // volume.profile.TryGet(out Vignette c);
        //         // print(c.intensity.value);
        //         // c.intensity.value = Mathf.Abs((1 - Timekeeper.instance.Clock(mainClockName).localTimeScale)/2.0f);
        //     }
        // }
        // else
        // {
        //     Timekeeper.instance.Clock(mainClockName).paused = false;
        // }

        // Replaced all the previous crappy if with this
        //Timekeeper.instance.Clock(mainClockName).paused = !thePlayer.GetComponent<PlayerMovement>().Alive;


        
        
        

        // Power: Pause
        if (Input.GetKeyDown(KeyCode.P) && PowerCanPause)
        {
            CurrentPower = TimePower.PAUSE;
            Timekeeper.instance.Clock("EverythingElseClock").paused = true;
        }


        // Power: Rewinds everything
        else if (Input.GetKey(KeyCode.R) && PowerCanRewind)
        {
            CurrentPower = TimePower.REWIND;
            Timekeeper.instance.Clock(mainClockName).paused = false;
            Timekeeper.instance.Clock(mainClockName).localTimeScale = 1;
            Timekeeper.instance.Clock("PlayerClock").localTimeScale = -1;
            Timekeeper.instance.Clock("EverythingElseClock").localTimeScale = -1;
            Timekeeper.instance.Clock("EverythingElseClock").paused = false;
            
            // Corrects dying vignette
            volume.profile.TryGet(out Vignette c);
            if (c.intensity.value > 0.05f)
            {
                c.intensity.value -= Time.deltaTime ;
            }
            else
            {
                c.intensity.value = 0.0f;
            }
            
            
            
        }

        // Power: Only the player rewinds
        // todo (prioritary): avoid to stop the rewinding while player is "inside" objects
        // todo (not so prioritary): lerp rewind (starts slow and gets faster gradually
        else if (Input.GetKey(KeyCode.T) && PowerCanSelfRewind)
        {
            CurrentPower = TimePower.SELF_REWIND;
            Timekeeper.instance.Clock(mainClockName).paused = false;
            Timekeeper.instance.Clock("PlayerClock").localTimeScale = -1;
            Timekeeper.instance.Clock("EverythingElseClock").paused = true;
        }

        // Power: Fast Forward
        // Fast forwarding in not a "real" time-based power, the instance just stops existing and reappears later
        else if ((Input.GetKey(KeyCode.F) && PowerCanFastForward) || 
                 (ffCount == 1 && Time.timeSinceLevelLoad - ffTime < 1f)
        )
        {
            if (PreviousPower != TimePower.TRAVEL_FORWARD)
            {
                // Hack manhoso para que o FF demore pelo menos meio minuto na primeira vez
                // para não deixar que a pedra mate
                // Solução para o futuro: detetar objetos e não deixar parar o poder a meio de algo que mate
                ffCount++;
                ffTime = Time.timeSinceLevelLoad;

                thePlayer.GetComponent<PlayerMovement>().StartForwarding(Timekeeper.instance.Clock(mainClockName).time);
            }

            CurrentPower = TimePower.TRAVEL_FORWARD;
        }

        
        
        // Not doing time-base mambo jambo
        else
        {
            if (thePlayer.GetComponent<PlayerMovement>().Alive)
            {
                 Timekeeper.instance.Clock(mainClockName).paused = false;

                 Timekeeper.instance.Clock("PlayerClock").localTimeScale = 1.0f;
                 Timekeeper.instance.Clock("EverythingElseClock").localTimeScale = 1.0f;
                 Timekeeper.instance.Clock("EverythingElseClock").paused = false;
            }

        }

        _changedPower = CurrentPower != PreviousPower;
        if (_changedPower)
        {
            _lastPowerChangeTime = Timekeeper.instance.Clock(mainClockName).unscaledTime;
        }

        UpdateTimePowerEffect();


    }


    public void SetTimeSpeed(float speed)
    {
        Timekeeper.instance.Clock("PlayerClock").localTimeScale = speed;
        Timekeeper.instance.Clock("EverythingElseClock").localTimeScale = speed;
    }
    

    [SerializeField] private Volume volume;
    private void UpdateTimePowerEffect()
    {
        if (CurrentPower == TimePower.REWIND)
        {
            var currentTime = Timekeeper.instance.Clock(mainClockName).unscaledTime;
            var effectTime = currentTime - _lastPowerChangeTime;

            volume.profile.TryGet(out ChromaticAberration c);
            c.intensity.value = Mathf.Abs(Mathf.Sin(effectTime));
        }
        else
        {
            volume.profile.TryGet(out ChromaticAberration c);
            if (c.intensity.value > 0.05f)
            {
                c.intensity.value -= 2.0f * Time.deltaTime;
            }
            else
            {
                c.intensity.value = 0;
            }
       
            
        }
        

        //if to make her self an hologram
        if(CurrentPower == TimePower.SELF_REWIND)
        {
            thePlayer.GetComponent<PlayerMovement>().ChangeMaterialHologram(true);

        }
        else
        {
            thePlayer.GetComponent<PlayerMovement>().ChangeMaterialHologram(false);

        }
    }




}
