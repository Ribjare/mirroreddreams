using Chronos;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticWobble : MonoBehaviour
{

    public bool dirRight = true;
    public float speedRight = 2.0f;

    public bool dirUp= true;
    public float speedUp = 2.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (dirRight)
            transform.Translate(Vector2.right * speedRight * Time.deltaTime);
        else
            transform.Translate(-Vector2.right * speedRight * Time.deltaTime);

        if (dirUp)
            transform.Translate(Vector2.up * speedUp * Time.deltaTime);
        else
            transform.Translate(-Vector2.up * speedUp * Time.deltaTime);


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("MovementEnd"))
        {
            dirRight = !dirRight;

        }
        if (other.gameObject.CompareTag("UpMovementEnd"))
        {
            dirUp = !dirUp;

        }

    }
    public bool getDirection()
    {
        return dirRight;
    }
    public float getSpeed()
    {
        return speedRight;
    }



    //// Is currently active and usable or not
    //// F: Do I really need this?
    //// F: I think so because of the loop feature (see attribute below)
    //// F: Someone talking to himself in code commentaries should seek counseling?
    //public bool ActiveMovement;


    //// Use like a switch (on/off)
    //// For a door, on may mean the door is opening and off that the door is closed
    //public bool CurrentState;


    //// ---------------------------------- Movement configuration ---------------------------------

    //// The amplitude of the movement
    //// We assume the original position is for the off state
    //// F: Would it be better if this parameters was the final position?
    //// F: I this it is better this way because we can move the whole scene and this still works.
    //[SerializeField] private float movementAmplitudeX;
    //[SerializeField] private float movementAmplitudeY;

    //// The time required to move from the original place to the desired location for the on state
    //// F: Would it be better to define a velocity instead?
    //[Min(0.01f)]
    //[SerializeField] private float movementTimeX = 1.0f;
    //[Min(0.01f)]
    //[SerializeField] private float movementTimeY = 1.0f;

    //// The movements loops, meaning that, when it is on, it switches between on and off states
    //[SerializeField] private bool loop;

    //// -------------------------------------------------------------------------------------------



    //// ----------------------- Self usage ------------------------

    //private Vector3 _initialPosition;

    //private Vector3 finalPositionX;
    //private Vector3 finalPositionY;

    //// This one is public in order to allow usage in the custom recorder
    ////[HideInInspector] 
    //[SerializeField] public float ElapsedPercentageX { get; private set; }
    //[SerializeField] public float ElapsedPercentageY { get; private set; }

    //// -----------------------------------------------------------


    //// Start is called before the first frame update
    //void Start()
    //{
    //    _initialPosition = transform.position;
    //    finalPositionX = new Vector3((_initialPosition.x + movementAmplitudeX), 0); 
    //    finalPositionY = new Vector3(0, (_initialPosition.y + movementAmplitudeY));
    //    //_initialPosition + new Vector3(0, movementAmplitudeY);
    //    ElapsedPercentageX = 0.0f;
    //    ElapsedPercentageY = 0.0f;
    //}


    //// Update is called once per frame
    //protected void Update()
    //{
    //    // If the object's time scale is negative it is being rewinded
    //    // which means no computation is required
    //    if (GetComponent<Timeline>().timeScale <= 0)
    //    {
    //        return;
    //    }


    //    if (!ActiveMovement)
    //    {
    //        return;
    //    }

    //    // Calculates the percentage of movement considering the time that had pass

    //    // Movement time = 100% of the movement
    //    // Delta t = X

    //    if (CurrentState)
    //    {
    //        ElapsedPercentageX += GetComponent<Timeline>().deltaTime / movementTimeX;
    //        ElapsedPercentageY += GetComponent<Timeline>().deltaTime / movementTimeY;
    //    }

    //    else
    //    {
    //        ElapsedPercentageX -= GetComponent<Timeline>().deltaTime / movementTimeX;
    //        ElapsedPercentageY -= GetComponent<Timeline>().deltaTime / movementTimeY;
    //    }




    //    // --------------------------------------------------------------------
    //    // Bounds
    //    // If you ever wonder if this can be moved to the blocks above, the answer is, it can't
    //    // As time may move backwards, its possible to achieve, for instance, elapsed percentage > 1.0 in both states

    //    if (ElapsedPercentageX > 1.0f)
    //    {
    //        if (loop)
    //        {
    //            Switch();
    //            ElapsedPercentageX = 2.0f - ElapsedPercentageX;
    //        }
    //        else
    //        {
    //            ElapsedPercentageX = 1.0f;
    //        }
    //    }

    //    else if (ElapsedPercentageX < 0.0f)
    //    {
    //        if (loop)
    //        {
    //            Switch();
    //            ElapsedPercentageX = -ElapsedPercentageX;
    //        }
    //        else
    //        {
    //            ElapsedPercentageX = 0.0f;
    //        }
    //    }

    //    ///////////////////
    //    if (ElapsedPercentageY > 1.0f)
    //    {
    //        if (loop)
    //        {
    //            Switch();
    //            ElapsedPercentageY = 2.0f - ElapsedPercentageY;
    //        }
    //        else
    //        {
    //            ElapsedPercentageY = 1.0f;
    //        }
    //    }

    //    else if (ElapsedPercentageY < 0.0f)
    //    {
    //        if (loop)
    //        {
    //            Switch();
    //            ElapsedPercentageY = -ElapsedPercentageY;
    //        }
    //        else
    //        {
    //            ElapsedPercentageY = 0.0f;
    //        }
    //    }

    //    // ----------------------------------------------------------



    //    // Applies the lerp for the calculated percentage
    //    var currentPositionX = Vector3.Lerp(new Vector3(_initialPosition.x, 0), finalPositionX, ElapsedPercentageX);
    //    var currentPositionY = Vector3.Lerp(new Vector3(0, _initialPosition.y), finalPositionY, ElapsedPercentageY);


    //    // And moves to the new position
    //    // Shouldn't this be applied to the rigid body?
    //    transform.position = currentPositionX + currentPositionY;

    //}


    //public void Trigger(TriggerActions action)
    //{

    //    // They say good OOP almost removes the usage of ifs, so probably this shouldn't be done like this

    //    if (action == TriggerActions.Activate)
    //    {
    //        ActiveMovement = true;
    //    }

    //    else if (action == TriggerActions.Deactivate)
    //    {
    //        ActiveMovement = false;
    //    }

    //    else if (action == TriggerActions.SwitchActivation)
    //    {
    //        SwitchActivation();
    //    }

    //    else if (action == TriggerActions.Switch)
    //    {
    //        Switch();
    //    }

    //    else if (action == TriggerActions.TurnOn)
    //    {
    //        CurrentState = true;
    //    }

    //    else if (action == TriggerActions.TurnOff)
    //    {
    //        CurrentState = false;
    //    }

    //    else
    //    {
    //        Debug.Log("What the hell have you done? Unknown enum state for MovableTrigger!");
    //    }

    //}


    //private void Switch()
    //{
    //    CurrentState = !CurrentState;
    //}

    //private void SwitchActivation()
    //{
    //    ActiveMovement = !ActiveMovement;
    //}

    //private void TimeBeforeToActivate()
    //{

    //}
}
