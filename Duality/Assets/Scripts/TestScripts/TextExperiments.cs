using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextExperiments : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
        
        
        int wordIndex = 1;
        
        // Color32 myColor32 = new Color32(0, 255, 0, 50);

        
        
        // var _text = GetComponent<TextMeshProUGUI>();
        // TMP_WordInfo info = _text.textInfo.wordInfo[wordIndex];
        // for (int i = 0; i < info.characterCount; ++i)
        // {
        //     int charIndex = info.firstCharacterIndex + i;
        //     int meshIndex = _text.textInfo.characterInfo[charIndex].materialReferenceIndex;
        //     int vertexIndex = _text.textInfo.characterInfo[charIndex].vertexIndex;
        //
        //     Color32[] vertexColors = _text.textInfo.meshInfo[meshIndex].colors32;
        //     vertexColors[vertexIndex + 0] = myColor32;
        //     vertexColors[vertexIndex + 1] = myColor32;
        //     vertexColors[vertexIndex + 2] = myColor32;
        //     vertexColors[vertexIndex + 3] = myColor32;
        // }
        //
        // _text.UpdateVertexData(TMP_VertexDataUpdateFlags.All);        
        
        
        
        
        
        
        
         

        
        
        
        

        // var v = t.textInfo.characterInfo;
        //
        // var words = t.textInfo.wordInfo;
        //
        // print(words.Length);
        //
        // print(words[1].GetWord());

        // for (int i = 0; i < words[1].characterCount; ++i)
        // {
        //     print(words[1].characterCount);
        //     int charIndex = words[1].firstCharacterIndex + i;
        //     int meshIndex = v[charIndex].materialReferenceIndex;
        //     int vertexIndex = v[charIndex].vertexIndex;
        //
        //     Color32[] vertexColors = t.textInfo.meshInfo[meshIndex].colors32;
        //     vertexColors[vertexIndex + 0] = myColor32;
        //     vertexColors[vertexIndex + 1] = myColor32;
        //     vertexColors[vertexIndex + 2] = myColor32;
        //     vertexColors[vertexIndex + 3] = myColor32;
        //     
        // }
        //
        // t.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

    }

    // Update is called once per frame
    void Update()
    {
        var t = GetComponent<TextMeshProUGUI>();
        
        t.ForceMeshUpdate();

        var textInfo = t.textInfo;

        for (int i = 0; i < textInfo.characterCount; i++)
        {
            var charInfo = textInfo.characterInfo[i];

            var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;

            for (int j = 0; j < 4; j++)
            {
                var orig = verts[charInfo.vertexIndex + j];
                verts[charInfo.vertexIndex + j] =
                    orig + new Vector3(0, Mathf.Sin(Time.time * 2f + orig.x * 0.01f) * 10f, 0);
            }

        }

        for (int i = 0; i < textInfo.meshInfo.Length; i++)
        {
            var meshInfo = textInfo.meshInfo[i];
            meshInfo.mesh.vertices = meshInfo.vertices;
            t.UpdateGeometry(meshInfo.mesh, i);
        }        
    }
}
