using Chronos;
using UnityEngine;

public class TestRotate : MonoBehaviour
{

    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,0,speed*GetComponent<Timeline>().deltaTime);
    }
}
