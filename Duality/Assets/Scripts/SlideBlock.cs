using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideBlock : MonoBehaviour
{
    [SerializeField] Vector3 movement;

    private Vector3 startPoint;
    private Vector3 endPoint;

    [SerializeField] private float movementSpeed = 1.0f;

    // moving = true means move to end point
    // moving = false means move to start point
    // in this behaviour, the block only stops at the start and the end
    private bool moving;


    void Start()
    {
        startPoint = transform.position;
        endPoint = startPoint + movement;
    }

    void Update()
    {
        var destination = moving ? endPoint : startPoint;
        transform.position = Vector3.MoveTowards(transform.position, destination, movementSpeed * Time.deltaTime);
    }


    void OnMouseDown ()
    {
        Toggle();
    }

    public void Toggle ()
    {
        moving = !moving;
    }

    public void SetOn ()
    {
        moving = true;
    }

    public void SetOff ()
    {
        moving = false;
    }

}
