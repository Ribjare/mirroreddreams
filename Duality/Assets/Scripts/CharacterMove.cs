using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum CharacterState
{
    MOVING_LEFT,
    MOVING_RIGHT
}




public class CharacterMove : MonoBehaviour
{

    [SerializeField] private float rotationSpeed;

    [SerializeField] private CharacterState currentState;

    [SerializeField] private float movementSpeed;

    [SerializeField] private float world;

    // Start is called before the first frame update
    void Start()
    {
        currentState = CharacterState.MOVING_RIGHT;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateColliders();
        Movement();
        Direction();
        CheckConstraints();
        KeyInputs();
    }

    private void KeyInputs()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }


    // Sorry about the copy-paste... 
    private BoundingSphere frontCollisions1 = new BoundingSphere { radius = 0.2f };
    private BoundingSphere frontCollisions2 = new BoundingSphere { radius = 0.2f };
    private BoundingSphere frontCollisions3 = new BoundingSphere { radius = 0.2f };


    private BoundingSphere floorDetection = new BoundingSphere { radius = 0.1f };

    private void UpdateColliders()
    {
        frontCollisions1.position = transform.position + new Vector3(currentState == CharacterState.MOVING_RIGHT? 0.1f: -0.1f , 0.95f * world, 0);
        frontCollisions2.position = frontCollisions1.position + new Vector3(0, 0.7f, 0);
        frontCollisions3.position = frontCollisions1.position - new Vector3(0, 0.7f, 0);

        floorDetection.position = transform.position + new Vector3(currentState == CharacterState.MOVING_RIGHT ? 0.4f : -0.4f, -0.1f * world, 0);
    }

    private void CheckConstraints()
    {
        if (Physics.CheckSphere(frontCollisions1.position, frontCollisions1.radius, LayerMask.GetMask("Block")) ||
            Physics.CheckSphere(frontCollisions2.position, frontCollisions2.radius, LayerMask.GetMask("Block")) ||
            Physics.CheckSphere(frontCollisions3.position, frontCollisions3.radius, LayerMask.GetMask("Block")))
        {
            TurnAround();
        }

        else if (!Physics.CheckSphere(floorDetection.position, floorDetection.radius, LayerMask.GetMask("Block")))
        {
            TurnAround();
        }


    }



    private void FixedUpdate()
    {
        // Manual gravity (easier than we thought)
        GetComponent<Rigidbody>().AddForce(Physics.gravity * GetComponent<Rigidbody>().mass * world);
    }


    private void TurnAround()
    {
        currentState = currentState == CharacterState.MOVING_RIGHT ? CharacterState.MOVING_LEFT : CharacterState.MOVING_RIGHT;
    }


    private void Movement ()
    {
        var dir = currentState == CharacterState.MOVING_RIGHT ? 1.0f : -1.0f;
        transform.Translate(Time.deltaTime * movementSpeed * dir, 0, 0, Space.World);
    }

    private void Direction ()
    {
        // Corrects to where the player is facing
        var desiredRotation = currentState == CharacterState.MOVING_RIGHT ? new Vector3(0, 0, 0) : new Vector3(0, 180, 0);
        var currentRotation = transform.rotation.eulerAngles;
        var f = Vector3.Lerp(currentRotation, desiredRotation, Time.deltaTime * rotationSpeed);
        transform.rotation = Quaternion.Euler(f);
    }

    private void OnDrawGizmos()
    {
        UpdateColliders();
        Gizmos.DrawSphere(frontCollisions1.position, frontCollisions1.radius);
        Gizmos.DrawSphere(frontCollisions2.position, frontCollisions2.radius);
        Gizmos.DrawSphere(frontCollisions3.position, frontCollisions3.radius);
        Gizmos.DrawSphere(floorDetection.position, floorDetection.radius);
    }




}
