using System.Collections;
using System.Collections.Generic;
using Chronos;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {

        // Not working!

        if (other.CompareTag("Player"))
        {
            other.GetComponent<Timeline>().Reset();
            foreach (var tl in other.GetComponentsInChildren<Timeline>())
            {
                tl.ResetRecording();
                //tl.Reset();
            }
        }

    }
}
