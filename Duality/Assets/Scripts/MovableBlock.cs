using System;
using Chronos;
using UnityEngine;


public enum TriggerActions
{
    Activate,
    Deactivate,
    SwitchActivation,
    Switch,
    TurnOn,
    TurnOff
}


public class MovableBlock : MonoBehaviour
{
    // Is currently active and usable or not
    // F: Do I really need this?
    // F: I think so because of the loop feature (see attribute below)
    // F: Someone talking to himself in code commentaries should seek counseling?
    public bool ActiveMovement;


    // Use like a switch (on/off)
    // For a door, on may mean the door is opening and off that the door is closed
    public bool CurrentState;


    // ---------------------------------- Movement configuration ---------------------------------

    // The amplitude of the movement
    // We assume the original position is for the off state
    // F: Would it be better if this parameters was the final position?
    // F: I this it is better this way because we can move the whole scene and this still works.
    [SerializeField] private Vector3 movementAmplitude;

    // The time required to move from the original place to the desired location for the on state
    // F: Would it be better to define a velocity instead?
    [Min(0.01f)]
    [SerializeField] private float movementTime = 1.0f;

    // The movements loops, meaning that, when it is on, it switches between on and off states
    [SerializeField] private bool loop;


    [SerializeField] private bool deathOnFinish;



    // -------------------------------------------------------------------------------------------



    // ----------------------- Self usage ------------------------

    private Vector3 _initialPosition;

    private Vector3 _finalPosition;

    // This one is public in order to allow usage in the custom recorder
    //[HideInInspector] 
    [SerializeField] public float ElapsedPercentage { get; private set; }

    // -----------------------------------------------------------


    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = transform.position;
        _finalPosition = _initialPosition + movementAmplitude;
        ElapsedPercentage = 0.0f;
    }


    // Update is called once per frame
    protected void Update()
    {
        // If the object's time scale is negative it is being rewinded
        // which means no computation is required
        if (GetComponent<Timeline>().timeScale <= 0)
        {
            return;
        }


        if (!ActiveMovement)
        {
            return;
        }

        // Calculates the percentage of movement considering the time that had pass

        // Movement time = 100% of the movement
        // Delta t = X

        if (CurrentState)
        {
            ElapsedPercentage += GetComponent<Timeline>().deltaTime / movementTime;
        }

        else
        {
            ElapsedPercentage -= GetComponent<Timeline>().deltaTime / movementTime;
        }




        // --------------------------------------------------------------------
        // Bounds
        // If you ever wonder if this can be moved to the blocks above, the answer is, it can't
        // As time may move backwards, its possible to achieve, for instance, elapsed percentage > 1.0 in both states
       

        if (ElapsedPercentage > 1.0f)
        {
            if (loop)
            {
                Switch();
                ElapsedPercentage = 2.0f - ElapsedPercentage;
            }
            else
            {
                ElapsedPercentage = 1.0f;
                if(deathOnFinish)
                {
                    DeathFunction();
                }
            }
        }

        else if (ElapsedPercentage < 0.0f)
        {
            if (loop)
            {
                Switch();
                ElapsedPercentage = -ElapsedPercentage;
            }
            else
            {
                ElapsedPercentage = 0.0f;
            }
        }

        // ----------------------------------------------------------



        // Applies the lerp for the calculated percentage
        var currentPosition = Vector3.Lerp(_initialPosition, _finalPosition, ElapsedPercentage);


        // And moves to the new position
        // Shouldn't this be applied to the rigid body?
        transform.position = currentPosition;

    }

    private void DeathFunction()
    {
        ///ToDo: recive and script and activate the function related in order to be more generic
        Destroy(this.gameObject);
    }


    public void Trigger(TriggerActions action)
    {

        // They say good OOP almost removes the usage of ifs, so probably this shouldn't be done like this

        if (action == TriggerActions.Activate)
        {
            ActiveMovement = true;
        }

        else if (action == TriggerActions.Deactivate)
        {
            ActiveMovement = false;
        }

        else if (action == TriggerActions.SwitchActivation)
        {
            SwitchActivation();
        }

        else if (action == TriggerActions.Switch)
        {
            Switch();
        }

        else if (action == TriggerActions.TurnOn)
        {
            CurrentState = true;
        }

        else if (action == TriggerActions.TurnOff)
        {
            CurrentState = false;
        }

        else
        {
            Debug.Log("What the hell have you done? Unknown enum state for MovableTrigger!");
        }

    }


    private void Switch()
    {
        CurrentState = !CurrentState;
    }

    private void SwitchActivation()
    {
        ActiveMovement = !ActiveMovement;
    }

    private void TimeBeforeToActivate()
    {
        
    }
}
