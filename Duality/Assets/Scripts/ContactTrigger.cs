using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactTrigger : MonoBehaviour
{

    [SerializeField] private MovableBlock movableBlock;

    [SerializeField] private bool reverse;


    private void OnCollisionEnter2D(Collision2D col)
    {
        movableBlock.Trigger(reverse ? TriggerActions.TurnOn : TriggerActions.TurnOff);
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        movableBlock.Trigger(reverse ? TriggerActions.TurnOff : TriggerActions.TurnOn);
    }


}
