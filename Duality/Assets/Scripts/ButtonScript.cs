using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script controlling the button object
/// </summary>
public class ButtonScript : MonoBehaviour
{
    public bool active = false;
    public Animator animator;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Activate();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Activate");

            Activate();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Deactivate");
            Deactivate();
        }
    }


    /// <summary>
    /// Activates object
    /// </summary>
    public void Activate()
    {
        active = true;
        animator.SetBool("pressed", active);
        
    }

    /// <summary>
    /// Deactivates object
    /// </summary>
    public void Deactivate()
    {
        active = false;

        animator.SetBool("pressed", active);
    }
}
