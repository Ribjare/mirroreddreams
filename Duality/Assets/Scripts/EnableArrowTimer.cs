using System.Collections;
using System.Collections.Generic;
using Chronos;
using UnityEngine;

public class EnableArrowTimer : MonoBehaviour
{
    [SerializeField] private TimerTrigger timerTrigger;
    [SerializeField] private Timeline timeLine;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        timerTrigger.Delay = timeLine.time;
    }
}
