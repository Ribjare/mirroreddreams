using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerKeyBox : MonoBehaviour
{
    [SerializeField]
    private GameObject key;

    private Inventory inventory;

    private void Start()
    {
        inventory = FindObjectOfType<Inventory>();    
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(key == null)
        {
            Debug.LogError("N�O H� CHAVE NESTA CAIXA!!!!");
            return;
        }

        key.SetActive(true);
        inventory.hasLigthKey = true;
    }
}
