using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonType
{
    Toggle,
    On,
    Off
}

public class TriggerToggleSlide : MonoBehaviour
{
    [SerializeField] private SlideBlock[] triggables;
    [SerializeField] private ButtonType buttonType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        foreach (var s in triggables)
        {
            if (buttonType == ButtonType.Toggle)
            {
                s.Toggle();
            }

            else if (buttonType == ButtonType.On)
            {
                s.SetOn();
            }

            else
            {
                s.SetOff();
            }
        }
    }
}
