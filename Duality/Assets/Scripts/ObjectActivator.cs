using UnityEngine;

/// <summary>
/// This script is used to activate or deactivate objects
/// Is primarily used to change the the active virtal camera
/// 
/// To use: 
///     1 - Create a empty object as a son of Trigger object(this to stay organized), if is not avaible this object, you must create.
///  Name it in this form: Trigger - *Purpose of trigger* - *Which cameras*, ex: Trigger - Intro Transition - Far To Near
///  (this nomenclature is based on the Lost Crypt Project)
///     2 - Add to the created object a collider and mark it as Trigger. Edit the collider to the desired size.
///     3 - Add this scrip to the object.
///     4 - Fill the activator tag with the disered activator.
///     5 - Fill the object list with all the objects that you want to activate
///     6 - ????
///     7 - Profit? Maybe 
/// </summary>
public class ObjectActivator : MonoBehaviour
{
    [Tooltip("Tag of the object that activates this script")]
    [SerializeField] private string activatorTag = null;

    [Tooltip("If you want to deactivate the object on exit")]
    [SerializeField] private bool deactivateOnExit = false;

    [Tooltip("List of objects that are activated in this script")]
    [SerializeField] protected GameObject[] objects = null;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(activatorTag))
        {
            foreach (var obj in objects)
                obj.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (deactivateOnExit && collision.CompareTag(activatorTag))
        {
            foreach (var obj in objects)
                obj.SetActive(false);
        }
    }
}