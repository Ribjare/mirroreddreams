using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEnabler : ObjectActivator
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (var obj in this.objects)
            obj.SetActive(false);
    }

}
