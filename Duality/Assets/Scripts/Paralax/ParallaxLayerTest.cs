using UnityEngine;

/// <summary>
/// Paralax Script
/// 
/// This script functions moving a asset in a different speed of the main camera/playable area
/// 
/// </summary>
public class ParallaxLayerTest: MonoBehaviour
{
    [Tooltip("Speed multiplier of the layer comparing to the speed of the camera")]
    [SerializeField] float multiplier = 0.0f;

    [Tooltip("If the paralax only affects horizontal movement")]
    [SerializeField] bool horizontalOnly = true;

    private Transform cameraTransform;

    private Vector3 startCameraPos;
    private Vector3 startPos;

    void Start()
    {
        cameraTransform = Camera.main.transform;
        startCameraPos = cameraTransform.position;
        startPos = transform.position;
    }


    private void LateUpdate()
    {
        var position = startPos;
        if (horizontalOnly)
            position.x += multiplier * (cameraTransform.position.x - startCameraPos.x);
        else
            position += multiplier * (cameraTransform.position - startCameraPos);

        transform.position = position;
    }

}