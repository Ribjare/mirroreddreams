using Chronos;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// Script that calculates the time that until the movable object can move
/// 
/// </summary>
public class RewindTimerForMovableBlock : MonoBehaviour
{
    private Clock clock;
    private float waitTime;
    private float startWaitTime;
    private float endWaitTime;
    private MovableBlock blockScript;
    private bool reversed;

    // Start is called before the first frame update
    void Start()
    {
        clock = Timekeeper.instance.Clock("EverythingElseClock");
        blockScript = GetComponent<MovableBlock>();
        if (!blockScript)
        {
            Debug.LogError("NEEDS MOVABLE BLOCK TO WORK");
        }
        reversed = false;
    }

    // Update is called once per frame
    protected void Update()
    {
       //Gets the time that starts rewinding in place
        if (clock.state == TimeState.Reversed)//If time is reversing
        {
            if (blockScript.ElapsedPercentage == 0)//If it's stopped moving
            {
                if(startWaitTime == 0)
                {
                    startWaitTime = clock.time;//Gets the absolute time
                    endWaitTime = 0;
                    blockScript.ActiveMovement = false;
                    reversed = true;


                }
            }
        }
        if(clock.state == TimeState.Normal)//If time is normal
        {   
            //Gets the end time and calculates the wait time
            if (endWaitTime == 0) { 
                endWaitTime = clock.time;
                waitTime =Mathf.Abs(endWaitTime - startWaitTime);
                startWaitTime = 0;
               
            }

            waitTime -= clock.deltaTime;//Cooldown for activating

            // When wait is <= 0 the movement is equal 
            if(waitTime <= 0 && reversed)
            {
                blockScript.ActiveMovement = true;
            }

        }
    }
}
