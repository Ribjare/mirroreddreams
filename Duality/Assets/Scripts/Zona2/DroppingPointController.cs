using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller that activates all the dropping points
/// </summary>
public class DroppingPointController : MonoBehaviour
{
    [Tooltip("List of possible droping point of the droplets")]
    public List<DropingRockController> droppingPoints; // ToDo: automaticly creates dropping points to put in scene

    [Tooltip("Max time between each droplet activates")]
    public float time = 3;

    [SerializeField]
    private GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        foreach (DropingRockController point in droppingPoints)
        {
            point.CreateFirstDroppingRock(prefab, time);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /**
         * 
        
        if (Random.Range(0f, 100f) <= 10)
        {
             DropingRockController dropPoint = GetRandomLocation();
             dropPoint.CreateDroppingRock(prefab, time);
        } */
    }

    /// <summary>
    /// Choose the point to rock at random
    /// </summary>
    /// <returns>DropingRockController of a point</returns>
    private DropingRockController GetRandomLocation()
    {
        // Gets a random poin;
        var randomIndex = Random.Range(0, droppingPoints.Count);
        var point = droppingPoints[randomIndex];

        return point;
    }

    //Deactivates all the points
    public void DeactivateAll()
    {
        foreach (DropingRockController point in droppingPoints)
        {
            point.Deactivate();
        }
    }

}

