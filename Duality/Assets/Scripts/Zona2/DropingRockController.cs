using Chronos;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropingRockController : MonoBehaviour
{
    public float maxTime = 5f;
    private float timeLeft = 2f;
    private bool active = false;
    private GameObject prefab;
    private Clock clock;

    private void Start()
    {
        clock = Timekeeper.instance.Clock("EverythingElseClock");
    }
    // Update is called once per frame
    void Update()
    {
        if (clock.state != TimeState.Reversed)//Stops spawning more if we are rewinding
        {
            timeLeft -= Time.deltaTime;
            if (active && timeLeft < 0)
            {
              CreateDroppingRock(prefab, GetRandomTime()); //
            }
        }
    }
    /// <summary>
    /// Starts the dropping rocks
    /// </summary>
    /// <param name="prefab">The prefab thats gets created to drop</param>
    /// <param name="time">Max time betwen each droplet</param>
    public void CreateFirstDroppingRock(GameObject prefab, float time)
    {
        this.maxTime = time;

        CreateDroppingRock(prefab, 0f);
    }

    /// <summary>
    /// 
    /// Instantiate a new droping rock
    /// 
    /// </summary>
    private void CreateDroppingRock(GameObject prefab, float time)
    {
        var pointTranform = this.transform;
        var rotation = pointTranform.rotation * Quaternion.Euler(0f, 0f, 0f);
        var position = pointTranform.position;

        Instantiate(prefab, position, rotation, pointTranform);

        timeLeft = time;
        Activate();
        this.prefab = prefab;
    }

    public void Activate()
    {
        active = true;
    }
    public void Deactivate()
    {
        active = false;
    }

    private float GetRandomTime()
    {
      return Random.Range(0f, maxTime);
    }
}
