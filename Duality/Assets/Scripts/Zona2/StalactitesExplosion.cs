using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalactitesExplosion : MonoBehaviour
{
    private SpriteRenderer thisSpriteRendered;
    private bool destoyed = false;

    private void Start()
    {
        thisSpriteRendered = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {

            DestroyEffect();
        }
    }
    public void DestroyEffect()
    {
        //TODO particles effect
        thisSpriteRendered.enabled = false;
        //Destroy(gameObject);
    }

}
