using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        var component = other.GetComponent<StalactitesExplosion>();
        Debug.Log(other.name);
        if (component)
        {
            component.DestroyEffect();
        }

    }
}
