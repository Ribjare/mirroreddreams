using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuckPlayerToMovableObject : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("OOOOOO");

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.parent = transform;
            collision.rigidbody.interpolation = RigidbodyInterpolation2D.None;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.parent = null;
            collision.rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;

        }
    }
}
