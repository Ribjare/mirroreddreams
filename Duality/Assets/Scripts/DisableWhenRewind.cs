using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableWhenRewind : MonoBehaviour
{

    public List<MonoBehaviour> components;

    public void Disable()
    {
        foreach(Component aux in components)
        {
            Destroy(aux);
        }
    }
}
