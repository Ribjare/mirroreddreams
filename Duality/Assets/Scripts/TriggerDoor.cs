using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDoor : MonoBehaviour
{
    [SerializeField]
    private bool open = false;
    [SerializeField]
    private GameObject door;

    
    public bool key = false;
    private Inventory inventory;


    private void Start()
    {
        inventory = FindObjectOfType<Inventory>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (inventory.hasLigthKey) { 
            door.SetActive(false);
            open = false;
        }
    }
}
