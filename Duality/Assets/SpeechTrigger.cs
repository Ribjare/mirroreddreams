using UnityEngine;

// [Serializable]
// public class SpeechClip
// {
//     public string SpeechTextKey;
//     public AudioClip SpeechAudio;
//     public float SpeechDuration = 0.0f;
// }



public class SpeechTrigger : MonoBehaviour
{
    //public SpeechClip[] SpeechClips;
    
    
    public string SpeechTextKey;

    public AudioClip SpeechAudio;

    // Leave it as zero or negative to automatically get the duration from the AudioClip
    public float SpeechDuration = 0.0f;

    public GameObject ActivateThisAtTheEndOfText;

    public GameObject ActivateThisAtTheStartOfText;
    
    
    
    public bool StopsCharacter = false;

    public bool ResumesCharacterInTheEnd = false;

    public PlayerMovement Character;

    private bool used;
    
    private void Start ()
    {
        used = false;
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (ActivateThisAtTheStartOfText != null)
        {

            ActivateThisAtTheStartOfText.SetActive(true);
        }


        if (used)
        {
            return;
        }

        if (StopsCharacter)
        {
            Character.CanControl = false;
        }
        
        used = true;
        if (SpeechDuration <= 0.0f && SpeechAudio != null)
        {
            SpeechDuration = SpeechAudio.length;
        }
        SpeechManager.Instance.Display(ParseLanguageCSV.GetString(SpeechTextKey), SpeechDuration, 
            SpeechAudio, ResumesCharacterInTheEnd, ActivateThisAtTheEndOfText);
        
    }
    
    
    
    
}
