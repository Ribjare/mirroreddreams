using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedActivation : MonoBehaviour
{

    public GameObject ActivateThis;
    public float Delay;
    
    
    public void Update()
    {
        
    }
    
    
    // Corrigir isto para funcionar com o tempo interno do jogo (e não com corotina)
    // (Solução para desenrascar a 10 de junho!)
    
    public void OnTriggerEnter2D(Collider2D c)
    {
        Invoke("Activate", Delay);
    }

    public void Activate()
    {
        ActivateThis.SetActive(true);
    }
}
