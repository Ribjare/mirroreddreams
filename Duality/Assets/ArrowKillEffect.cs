using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public enum PowerActivation
{
    Rewind,
    Forward,
}
public class ArrowKillEffect : MonoBehaviour
{

    private MovableBlock _movement;

    public GameObject ThePlayer; //PLayer prefab

    public GameObject KillerTrigger; 

    private float _maxDistance;
    
    [SerializeField] private Volume volume;

    public float OffsetDistance;

    public GameObject ActivateThis;

    private bool used;

    private float TimeOfDeath = -1;

    private void Start()
    {
        // If volume isn't manually picked goes for the default one
        if (!volume) { 
            volume = GameObject.Find("Global Volume").GetComponent<Volume>();
        }
    }
    private void OnTriggerEnter2D(Collider2D c)
    {
        if(c.CompareTag("Player"))
            TimeOfDeath = Time.timeSinceLevelLoad;
    }


    void Update()
    {
        if (TimeOfDeath > 0)
        {
            var speed = 1 - (Time.timeSinceLevelLoad - TimeOfDeath) * 5.0f;
            if (speed < 0.05f)
            {
                speed = 0.0f;
               // TimeControl.Instance.PowerCanRewind = true;
                if(ActivateThis != null)
                    ActivateThis?.SetActive(true); //If has something to activate, it activates
                TimeOfDeath = -1;
            }
            TimeControl.Instance.SetTimeSpeed(speed);
            volume.profile.TryGet(out Vignette c);
            c.intensity.value = 0.55f * (1 - speed);

            // if (TimeOfDeath < Time.timeSinceLevelLoad - 0.05f)
            // {
            //     TimeControl.Instance.SetTimeSpeed(0.0f);
            // }
        }
        
    }
    
    // Start is called before the first frame update
    // void Start()
    // {
    //     _movement = GetComponent<MovableBlock>();
    //     _maxDistance = Vector3.Distance(KillerTrigger.transform.position, transform.position);
    //     used = false;
    // }

    // // Update is called once per frame
    // void Update()
    // {
    //
    //     // Se o tempo estiver a andar para trás e a distância for grande então pode-se remover este efeito
    //     // if (TimeControl.Instance.CurrentPower == TimePower.REWIND)
    //     // {
    //     //     var d= Vector3.Distance(ThePlayer.transform.position, transform.position) - OffsetDistance;
    //     //     
    //     //     // Slowdown time with the distance
    //     //     var p = d / _maxDistance;
    //     //
    //     //     print(p);
    //     //     
    //     //     if (p < 0)
    //     //     {
    //     //         volume.profile.TryGet(out Vignette c);
    //     //         c.intensity.value = 0;
    //     //         Destroy(this);
    //     //     }
    //     // }
    //     
    //     
    //     if (_movement.ActiveMovement)
    //     {
    //
    //         //used = true;
    //         TimeControl.Instance.PowerCanRewind = true;
    //         ActivateThis.SetActive(true);
    //         
    //         
    //         
    //         var d= Vector3.Distance(ThePlayer.transform.position, transform.position) - OffsetDistance;
    //         
    //         // Slowdown time with the distance
    //         var p = d / _maxDistance;
    //         
    //         
    //         if (p >= 1)
    //         {
    //             p = 1;
    //         }
    //
    //         if (p < 0)
    //         {
    //             p = 0;
    //         }
    //         
    //         TimeControl.Instance.SetTimeSpeed(p);
    //
    //         volume.profile.TryGet(out Vignette c);
    //         c.intensity.value = (1 - p)*0.6f;
    //         
    //         
    //
    //         
    //     }
    //     
    //     else if (used)
    //     {
    //         
    //         // Já foi usado e não está ativo, significa que retrocedeu
    //         Destroy(this);
    //     
    //     
    //     }
    //
    //     
    //     
    // }
}
