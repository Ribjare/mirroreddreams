using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CollectiblesNavigation : MonoBehaviour
{
    private int CurrentPage = 0;

    private List<Tuple<string, Sprite>> UnlockedPages = new List<Tuple<string, Sprite>>();

    public TextMeshProUGUI TextComponent;
    public Image SpriteComponent;

    public GameObject ButtonLeft;
    public GameObject ButtonRight;
    

    // Start is called before the first frame update
    void Start()
    {
        UpdatePageContent();
    }

    // Update is called once per frame
    void Update()
    {
        ButtonLeft.SetActive(CanMoveLeft());
        ButtonRight.SetActive(CanMoveRight());
        
        if (Input.GetKeyDown(KeyCode.A))
        {
            PreviousPage();
        }
        
        else if (Input.GetKeyDown(KeyCode.D))
        {
            NextPage();
        }
    }


    private bool CanMoveLeft()
    {
        return CurrentPage > 0;
    }

    private bool CanMoveRight()
    {
        return CurrentPage < UnlockedPages.Count - 1;
    }
    
    private void NextPage()
    {
        if (CanMoveRight())
        {
            CurrentPage++;
            UpdatePageContent();
        }
    }

    private void PreviousPage()
    {
        if (CanMoveLeft())
        {
            CurrentPage--;
            UpdatePageContent();
        }
    }

    private void UpdatePageContent()
    {
        if (UnlockedPages.Count > 0)
        {
            var t = UnlockedPages[CurrentPage];
            TextComponent.text = t.Item1;
            SpriteComponent.sprite = t.Item2;
        }

    }

    public void UnlockPage(string text, Sprite image)
    {
        UnlockedPages.Add((new Tuple<string, Sprite>(text, image)));
        CurrentPage = UnlockedPages.Count - 1;
        UpdatePageContent();
    }
}
