
using TMPro;
using UnityEngine;

public class SpeechManager : MonoBehaviour
{
    
    private static readonly float EXTRA_TIME = 0.25f;
    
    
    
    public float DisplayTime = 5.0f;

    private float currentTime;

    public TextMeshProUGUI TextComponent;

    public GameObject RootElement;

    public AudioSource AudioSourceComponent;

    public static SpeechManager Instance;

    public bool CharacterShouldPause = false;

    public PlayerMovement PlayerCharacter;

    private bool resumesPlayerInTheEnd;

    private GameObject _activateAtTheEnd;
    
    
    // Start is called before the first frame update
    SpeechManager()
    {
        if (Instance != null)
        {
            Debug.Log("You are messing with the Singletons. Old instance will be replaced");
        }
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        // We might have to check this
        // Everything that has to do with time is dangerous
        
        // In the first n miliseconds, fades in
        
        
        // In the last n miliseconds, fades out

        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentTime = DisplayTime;
        }
        
        currentTime += Time.deltaTime;

        if (currentTime >= DisplayTime)
        {
            RootElement.SetActive(false);
            if (resumesPlayerInTheEnd)
            {
                PlayerCharacter.CanControl = true;
            }

            if (_activateAtTheEnd != null)
            {
                _activateAtTheEnd.SetActive(true);
            }

        }
    }


    public void Display(string message, float duration, 
        AudioClip soundClip = null, bool resumePlayer = false, GameObject activateAtTheEnd = null)
    {
        currentTime = 0;
        DisplayTime = duration + EXTRA_TIME;
        TextComponent.text = message;
        RootElement.SetActive(true);
        resumesPlayerInTheEnd = resumePlayer;
        _activateAtTheEnd = activateAtTheEnd;
        
        if (soundClip != null)
        {
            if (AudioSourceComponent.isPlaying)
            {
                AudioSourceComponent.Stop();
            }

            AudioSourceComponent.clip = soundClip;
            AudioSourceComponent.Play();
        }

        if (CharacterShouldPause)
        {
            // Pause the character!!
            // 
        }

    }
    
    
}
