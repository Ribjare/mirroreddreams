using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class Collect : MonoBehaviour
{
    public string TextKey;
    public Sprite Image;
    public CollectEvent CollectHandler;
    public CollectiblesNavigation CollectiblesManager;
    public float EffectFactor = -10f;
    
    public delegate void CollectEvent();


    private bool _collected;
    
    // Start is called before the first frame update
    void Start()
    {
        _collected = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_collected)
        {
            // Time.deltaTime???? Maybe not because of the time management of the game
            transform.localScale *= 1 + EffectFactor * Time.deltaTime;
            //transform.Translate(0,0, 10 * Time.deltaTime);
            
            
            // Destroys if it has a very small or large scale

            if (transform.localScale.x > 20)
            {
                Destroy(gameObject);
            }
            
            if (transform.localScale.x < 0.01f)
            {
                Destroy(gameObject);
            }
        }
    }


    public void OnTriggerEnter2D(Collider2D c)
    {
        if (!_collected)
        {
            // Collects the Event
            CollectiblesManager.UnlockPage(ParseLanguageCSV.GetString(TextKey), Image);

            // If there is an event handler, call it
            CollectHandler?.Invoke();

            _collected = true;
        }
    }
    
    
    
}
