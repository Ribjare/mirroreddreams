using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseCollectibles : MonoBehaviour
{

    public PlayerMovement ThePlayerMovement;

    public void Start()
    {
        ThePlayerMovement.CanControl = false;
    }

    public void OnDisable()
    {
        ThePlayerMovement.CanControl = true;
    }

}
