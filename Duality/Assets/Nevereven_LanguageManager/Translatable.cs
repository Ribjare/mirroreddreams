﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;




public class Translatable : MonoBehaviour
{
    
    public string Key;

    // Start is called before the first frame update
    void Start()
    {
        UpdateTranslation();
    }

    private bool startedOnce = false;

    void OnEnable()
    {
        if (startedOnce)
        {
            UpdateTranslation();
        }
    }

    private void OnDisable()
    {
        startedOnce = true;
    }



    public void UpdateTranslation ()
    {
        var text = ParseLanguageCSV.GetString(Key);

        if (GetComponent<TextMeshProUGUI>() != null)
        {
            GetComponent<TextMeshProUGUI>().text = text;
        }
        
        else if (GetComponent<TextMeshPro>() != null)
        {
            GetComponent<TextMeshPro>().text = text;
        }
        
        else if (GetComponent<Text>() != null)
        {
            GetComponent<Text>().text = text;
        }

        else
        {
            Debug.LogError("Can't update text element. Doesn't have a supported text component for the text");
        }
        
    }


}
