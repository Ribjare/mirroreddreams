﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ParseLanguageCSV : MonoBehaviour
{
    public TextAsset TextFile;

    public string Language;

    public static Dictionary<string, string> Texts;

    public static ParseLanguageCSV Instance;

    public string[] ExistingLanguages { get; private set; }

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            LoadLanguage();
            DontDestroyOnLoad(this);            
        }
    }

    public void LoadLanguage ()
    {
        Texts = new Dictionary<string, string>();

        string textoWithNoSlashR = TextFile.text.Replace("\r", "");
        string[] allLines = textoWithNoSlashR.Split("\n"[0]);
        string firstLine = allLines[0];
        string[] splitedFirstLine = firstLine.Split(';');
        
        ExistingLanguages = new string[splitedFirstLine.Length-1];
        for (int i = 1; i < splitedFirstLine.Length;i++)
        {
            ExistingLanguages[i - 1] = splitedFirstLine[i];
        }
        
        
        int languageIndex = -1;
        for (int i = 1; i < splitedFirstLine.Length; i++)
        {
            if (splitedFirstLine[i] == Language)
            {
                languageIndex = i;
            }
        }

        if (languageIndex == -1)
        {
            Debug.LogError("Error Loading Language");
            return;
        }

        string[] line;
        for (int i = 1; i < allLines.Length; i++)
        {
            if (allLines[i].Length <= 2) continue;
            
            line = allLines[i].Split(';');
            if (languageIndex >= line.Length)
            {
                Debug.LogWarning("Warning. Could not read text line: " + allLines[i]);
            }
            else
            {
                Texts.Add(line[0], line[languageIndex]);
            }
        }
    }


    public static string GetString (string key)
    {
        return Texts.ContainsKey(key)?Texts[key]:key;
    }
    
    
    public static void UpdateAllTranslations ()
    {
        foreach (Translatable t in FindObjectsOfType<Translatable>())
        {
            t.UpdateTranslation();
        }
        
    }
   
}

