using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CheatManager : MonoBehaviour
{
    
    private List<string> input;
    
 
    void Awake() 
    {
        input = new List<string>();
    }
 
    void Update()
    {
        AddPressedKeys();
    }

    public bool ApplyCheats(string cheat)
    {
        if (TestCheat(cheat))
        {
            Debug.Log("Cheat: " + cheat);
            ClearCheat();
            return true;
        }
        return false;
    }

    private void AddPressedKeys()
    {
        for(int i = (int)KeyCode.A; i <= (int)KeyCode.Z; i++)
        {
            if (Input.GetKeyDown((KeyCode)i))
            {
                var offset = i - (int) KeyCode.A;
                var c = 'A';
                c+= (char)offset;
                input.Add(c.ToString());
            }
        }
    }

    private bool TestCheat(string code)
    {
        return string.Join("", input).EndsWith(code);
    }

    private void ClearCheat()
    {
        input.Clear();
    }
    


}
