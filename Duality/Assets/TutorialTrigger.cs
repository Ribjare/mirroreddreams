using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    public string TextKey;

    public Sprite[] Images;

    public TutorialMessage MessageContainer;

    private bool used;

    private void Start()
    {
        used = false;
    }
    
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!used)
        {
            used = true;
            MessageContainer.UpdateTutorialMessage(ParseLanguageCSV.GetString(TextKey), Images);
        }

    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        MessageContainer.HideTutorialMessage();
    }
    
    
}
